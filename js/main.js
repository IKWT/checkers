// Constructors

var Cell,
    Checker,
    Checkerboard,
    Game,
    game;

Cell = function(row, cell, playerChecker, className) {
    var domCell;

    domCell = document.createElement('td');

    domCell.row = row;

    domCell.cell = cell;

    domCell.coordinates = [row, cell];

    domCell.className = className;

    domCell.jumpTarget = false;

    domCell.hasPlayerChecker = playerChecker;

    domCell.available = false;

    return domCell;
}

Checker = function(row, cell, player, king) {
    var domChecker;

    domChecker = document.createElement('a');

    domChecker.row = row;

    domChecker.cell = cell;

    domChecker.coordinates = [row, cell];

    domChecker.targetToCut = false;

    domChecker.player = player;

    domChecker.king = king;

    if ( domChecker.player === 1 ) {
        domChecker.className = 'checkerWhite';
    } else {
        domChecker.className = 'checkerBlack';
    }

    if ( domChecker.king === true ) {
        domChecker.className += ' king';
    }

    domChecker.active = false;

    domChecker.setActive = function() {
            var checkers;

            checkers = document.getElementById('b-playfield').getElementsByTagName('a');

            if( this.active ) {
                this.className = this.className.replace('active', '').trim();

                this.active = false;
            } else {
                for ( var i = 0, length = checkers.length ; i < length ; i++) {
                    var checker = checkers[i];

                    if ( checker.active ) {
                        checker.active = false;
                    }
                    checker.className = checker.className.replace('active', '').trim();
                }

                this.active = true;

                this.className += ' active';
            }

        return this.coordinates;
    }

    domChecker.getAvailCells = function() {
        var roundCoords,
            lastRow = game.limits[0],
            lastCell = game.limits[1],
            cell,
            checker,
            neighborChecker,
            jumpCoords,
            player = this.player;

        roundCoords = [];

        jumpCoords = [];

        if ( this.king === true ) {
            for ( var i = 0, length = cells.length ; i < length ; i++) {
                cell = cells[i];

                if ( Math.abs( this.row - cell.coordinates[0] ) === Math.abs( this.cell - cell.coordinates[1]) ) {
                    roundCoords.push(cell.coordinates);
                }
            }
        } else {
            roundCoords = [[this.row + 1, this.cell - 1], [this.row + 1, this.cell + 1], [this.row - 1, this.cell + 1], [this.row - 1, this.cell - 1]];
        }

        for ( var i = 0 ; i < roundCoords.length ; i++) {
            if ( roundCoords[i][0] > lastRow || roundCoords[i][1] > lastCell || roundCoords[i][0] < 0 || roundCoords[i][1] < 0 ) {

                roundCoords.splice(i, 1);

                i--;
            }
        }

        // Set new available cells

        if ( this.king === true ) {

            for ( var i = 0, length = cells.length ; i < length ; i++) {
                cell = cells[i];

                cell.available = false;

                checker = cell.firstElementChild;

                for ( var j = 0, coordsLength = roundCoords.length ; j < coordsLength ; j++) {
                    if ( cell.hasPlayerChecker === 0 && cell.coordinates.compare(roundCoords[j]) ) {
                        cell.available = true;

                        cell.jumpTarget = true;

                    } else if ( cell.hasPlayerChecker !== 0 && cell.coordinates.compare(roundCoords[j]) ) {
                        cell.available = false;

                        neighborChecker = cell.firstElementChild;

                        if ( this.player !== neighborChecker.player ) {
                            neighborChecker.targetToCut = true;
                        }
                    }
                }
            }
        } else {
            for ( var i = 0, length = cells.length ; i < length ; i++) {
                cell = cells[i];

                cell.available = false;

                checker = cell.firstElementChild;

                for ( var j = 0, coordsLength = roundCoords.length ; j < coordsLength ; j++) {
                    if ( cell.hasPlayerChecker === 0 && cell.coordinates.compare(roundCoords[j]) ) {
                        cell.available = true;

                        if ( this.player === 1 ) {
                            if ( roundCoords[j][0] > this.row) {
                                 cell.available = false;
                            }

                        } else if ( this.player === 2 ) {
                            if ( roundCoords[j][0] < this.row) {
                                 cell.available = false;
                            }
                        }
                    } else if ( cell.hasPlayerChecker !== 0 && cell.coordinates.compare(roundCoords[j]) ) {
                        cell.available = false;

                        neighborChecker = cell.firstElementChild;

                        //console.log(neighborChecker);

                        if ( this.player !== neighborChecker.player ) {
                            neighborChecker.targetToCut = true;

                            var jumpCoord = [];

                            jumpCoord[0] = this.row + ( neighborChecker.row - this.row ) * 2;

                            jumpCoord[1] = this.cell + ( neighborChecker.cell - this.cell ) * 2;

                            jumpCoords.push(jumpCoord);
                        }
                    }
                }
            }

            for ( var i = 0, length = cells.length ; i < length ; i++) {
                cell = cells[i];

                if ( jumpCoords.length > 0 ) {
                    cell.jumpTarget = false;

                    for ( var k = 0, jumpCoordsLength = jumpCoords.length ; k < jumpCoordsLength ; k++) {
                        if ( cell.hasPlayerChecker === 0 && cell.coordinates.compare(jumpCoords[k]) ) {
                            this.readyToJump = true;

                            cell.available = true;

                            cell.jumpTarget = true;
                        }
                    }
                }
            }
        }
    }

    return domChecker;
}

Checkerboard = function() {
   var limits = [];

    this.layout = [
        [0,-1,0,-1,0,-1,0,-1],
        [-1,0,-1,0,-1,0,-1,0],
        [0,-1,0,-1,0,-1,0,-1],
        [2,0,2,0,2,0,2,0],
        [0,2,0,2,0,2,0,2],
        [1,0,1,0,1,0,1,0],
        [0,1,0,1,0,1,0,1],
        [1,0,1,0,1,0,1,0],
    ];

    this.createBoard = function() {
        var layout,
            board,
            row,
            cell,
            checker;

        layout = this.layout;

        board = document.createElement('table');

        for ( var i = 0, length =  layout.length ; i < length ; i++ ) {
            row = document.createElement('tr');

            for ( var j = 0, elemLength = layout[i].length ; j < elemLength ; j++ ) {

                if ( layout[i][j] === 0 ) {
                    cell = new Cell(i, j, 0, 'white');
                } else if ( layout[i][j] === -1 ) {
                    cell = new Cell(i, j, 2, 'black');

                    checker = new Checker(i, j, 2, false);

                    cell.appendChild(checker);
                } else if ( layout[i][j] === 1 ) {
                    cell = new Cell(i, j, 1, 'black');

                    checker = new Checker(i, j, 1, false);

                    cell.appendChild(checker);
                } else {
                    cell = new Cell(i, j, 0, 'black');
                }

                row.appendChild(cell);
            }

            board.appendChild(row);
        };

        limits[0] = i - 1;

        limits[1] = j - 1;

        return board;
    }

    this.limits = limits;
};

Game = function() {
    var board;

    board = new Checkerboard();

    this.limits = board.limits;

    this.activeCheckertCoords;

    this.turnToMove = 1;

    this.inMultiJump = false;

    this.kingMove = false;

    this.renderBoard = function(parent) {
        var field;

        field = board.createBoard();

        document.querySelector(parent).appendChild(field);
    };

    this.startGame = function(parent) {
        this.renderBoard(parent);
    };

    this.activateChecker = function(target) {

        if ( target.player !== this.turnToMove || target.active === true ) {
            return false;
        } else {

            this.activeCheckertCoords = target.setActive();

            target.getAvailCells();

            if ( target.king === true ) {
                this.kingMove = true;
            }

        };
    };

    this.moveTo = function(target) {
        if ( target.available === false ) {
            return false
        } else {
            var checker,
                currentRow = target.row,
                currentCell = target.cell,
                currentPlayer;

            for ( var i = 0, length = checkers.length; i < length ; i++ ) {
                if ( checkers[i].active ) {
                    currentPlayer = checkers[i].player;
                }
            }

            if ( ( currentPlayer == 1 && target.row == 0 ) || ( currentPlayer == 2 && target.row == 7 ) || this.kingMove === true ) {
                checker = new Checker(currentRow, currentCell, currentPlayer, true);
            } else  {
                checker = new Checker(currentRow, currentCell, currentPlayer, false);
            }

            target.appendChild(checker);

            target.hasPlayerChecker = currentPlayer;
        }

        this.deleteChecker(target);

        this.multiJumpCheck(target);

        this.changeTurn();
    };

    this.multiJumpCheck = function(target) {
        var newChecker;

        if( target.jumpTarget === true ) {
            this.inMultiJump = true;

            newChecker = target.firstElementChild;

            newChecker.getAvailCells();
            if ( newChecker.readyToJump === true ) {
                this.activateChecker(newChecker);

            } else {
                resetAvailable();

                this.inMultiJump = false;

                this.kingMove = false;
            }
        }
    };

    this.deleteChecker = function(target) {
        var activeCheckertCoords,
            targetToCutCoords = [],
            targetCoords;

        activeCheckertCoords = this.activeCheckertCoords;

        targetCoords = target.coordinates;

        if ( this.kingMove === true ) {
            var rowCoordDiff,
                cellCoordDiff,
                counter,
                acrossCoords = [];

                counter = Math.abs( targetCoords[0] - activeCheckertCoords[0] );

                console.log(counter);

                rowCoordDiff = ( targetCoords[0] - activeCheckertCoords[0] ) / counter;

                cellCoordDiff = ( targetCoords[1] - activeCheckertCoords[1] ) / counter;

                for ( var i = 0 ; i < counter; i++ ) {
                    var acrossCoord = [];

                    acrossCoord[0] = activeCheckertCoords[0] + rowCoordDiff * i;

                    acrossCoord[1] = activeCheckertCoords[1] + cellCoordDiff * i;

                    acrossCoords.push(acrossCoord);

                }

                for ( var i = 0 ; i < checkers.length ; i++) {
                    var checker = checkers[i];
                    for ( var j = 0 ; j < acrossCoords.length ; j++ ) {

                        if ( checker.active && target.available ) {
                            checker.parentNode.hasPlayerChecker = 0;

                            checker.parentNode.removeChild(checker);

                            target.available = false;

                            i--;
                        }

                        if ( checker.targetToCut === true && checker.coordinates.compare(acrossCoords[j]) && checker.parentNode ) {
                            checker.parentNode.hasPlayerChecker = 0;

                            checker.parentNode.removeChild(checker);

                            i--;
                        }
                    }
                }

                resetTargetToJump();

        } else {
            targetToCutCoords[0] = activeCheckertCoords[0] + (( target.coordinates[0] - activeCheckertCoords[0] ) / 2);
            
            targetToCutCoords[1] = activeCheckertCoords[1] + (( target.coordinates[1] - activeCheckertCoords[1] ) / 2);

            for ( var i = 0 ; i < checkers.length ; i++) {
                var checker = checkers[i];
                if ( checker.active && target.available ) {
                    checker.parentNode.hasPlayerChecker = 0;

                    checker.parentNode.removeChild(checker);

                    target.available = false;

                    i--;
                }

                if ( checker.targetToCut === true && checker.coordinates.compare(targetToCutCoords) ) {
                    checker.parentNode.hasPlayerChecker = 0;

                    checker.parentNode.removeChild(checker);

                    i--;
                } 
            }
        } 

        resetAvailable()

        this.kingMove = false;
    };

    this.changeTurn = function() {
        if ( this.inMultiJump === true ) {
            return false;
        }

        if ( this.turnToMove === 1 ) {
            this.turnToMove = 2;
        } else {
            this.turnToMove = 1;
        }
    }

    function resetAvailable() {
        for ( var i = 0, length = cells.length ; i < length ; i++) {
            var cell = cells[i];

            if ( cell.available === true ) {
                cell.available = false;
            }
        };
    };

    function resetTargetToJump() {
        for ( var i = 0, length = cells.length ; i < length ; i++) {
            var cell = cells[i];

            if ( cell.jumpTarget === true ) {
                cell.jumpTarget = false;
            }
        };
    };
}

// Game

game = new Game();

game.startGame('#b-playfield');

var checkers,
    cells;

checkers = document.getElementById('b-playfield').getElementsByTagName('a');

cells = document.getElementById('b-playfield').getElementsByTagName('td');

document.getElementById('b-playfield').addEventListener('click', function(event) {
    var target = event.target;

    if ( target.tagName == 'A' ) {
        event.stopPropagation();

        if (game.inMultiJump === true) {
            return false;
        }

        game.activateChecker(target);

    } else if ( target.tagName == 'TD' ) {

        game.moveTo(target);
    }
});


