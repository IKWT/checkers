
# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "css"
sass_dir = "sass"
images_dir = "images"
fonts_dir = "fonts"
http_generated_images_path = "../images/"
javascripts_dir = "js"
#output_style = :compressed
output_style = :expanded
line_comments = false

